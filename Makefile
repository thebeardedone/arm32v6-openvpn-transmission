-include env
IMAGENAME := $(shell basename `git rev-parse --show-toplevel`)
NAMESPACE := thebeardedone


.PHONY: help build run stop start rmf rmi

help:
	@echo "Copy the 'env.template' to 'env', modify it as you "
	@echo "please and run 'build' to build the image"


build:
	docker rmi -f $(NAMESPACE)/$(IMAGENAME):bak || true
	docker tag $(NAMESPACE)/$(IMAGENAME) $(NAMESPACE)/$(IMAGENAME):bak || true
	docker rmi -f $(NAMESPACE)/$(IMAGENAME) || true
	docker build -t $(NAMESPACE)/$(IMAGENAME) .


run:
	docker rm $(CONTAINER_NAME) || true
	docker run --cap-add=NET_ADMIN --device=/dev/net/tun -d --name $(CONTAINER_NAME) $(PORTS) $(VOLUMES) $(NAMESPACE)/$(IMAGENAME)


stop:
	docker stop $(CONTAINER_NAME)


start:
	docker start $(CONTAINER_NAME)


rmf:
	docker rm -f $(CONTAINER_NAME)


rmi:
	docker rmi $(NAMESPACE)/$(IMAGENAME)