# arm32v6-openvpn-transmission

This is an arm32v6 compatible alpine linux Docker image which runs transmission over an OpenVPN tunnel.

The structure of this project was inspired by and reused from [armhf-alpine-transmission](https://github.com/werwolfby/docker-armhf-alpine-transmission).

## Details
- [Source Repository](https://gitlab.com/thebeardedone/arm32v6-openvpn-transmission)
- [Dockerfile](https://gitlab.com/thebeardedone/arm32v6-openvpn-transmission/blob/master/Dockerfile)

## Running and configuring the transmission server
```bash
docker run --cap-add=NET_ADMIN --device=/dev/net/tun -d --name transmission \
  -p 9091:9091 \
  -p 51413:51413/tcp \
  -p 51413:51413/udp \
  -v <path_to_pi.ovpn_configuration_file>:/openvpn \
  -v <transmission_downloads_path>:/root/Downloads \
  -v <transmission_settings_path>:/etc/transmission \
  thebeardedone/arm32v6-openvpn-transmission
```

Stop the transmission container:
```bash
docker stop transmission
```

Edit file in `<transmission_settings_path>/settings.json` to configure transmission.
Specify *rpc_user* and *rpc_password*.

Start server:
```bash
docker run transmission
```

## Quick start

### Create image

Checkout this repository and run `make build`.
```bash
git clone https://gitlab.com/thebeardedone/arm32v6-openvpn-transmission
cd arm32v6-openvpn-transmission
make build
```

Copy `env.template` to `env`. Modify the environment variables as necessary.
The following lines need to be changed. Note that pi.ovpn has to exist in the OpenVPN path.
```bash
OPENVPN=<path_to_pi.ovpn_configuration_file>
DOWNLOADS=<transmission_downloads_path>
SETTINGS=<transmission_settings_path>
```

### Run container
```bash
make run
```

Stop the transmission container:
```bash
make stop
```

Edit file in `<transmission_settings_path>/settings.json` to configure transmission.
Specify *rpc_user* and *rpc_password*.

Start transmission again by running:
```bash
make start
```
