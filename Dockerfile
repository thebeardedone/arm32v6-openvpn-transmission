FROM arm32v6/alpine:3.7
MAINTAINER Ivo Zeba <croivozeba@hotmail.com>

RUN apk add --update bash openvpn transmission-daemon && \
	rm -rf /var/cache/apk/*

ADD update-resolv-conf /etc/openvpn/
ADD run.sh /tmp/

ENTRYPOINT "/tmp/run.sh"