# Start OpenVPN client
openvpn --config /openvpn/pi.ovpn &

sleep 5

# Get ip from configuration file and 
currentip=$(grep ipv4 /etc/transmission/settings.json | awk -F\" '{ print $4 }')
newip=$(ifconfig tun0 | grep 'inet addr:' | cut -d: -f2| cut -d' ' -f1)

if ifconfig tun0 >>/dev/null 2>&1; then

	# Set IP address if the ip from the configuration does not match that of tun0
	if [ "$currentip" != "$newip" ]; then
		sed -i "s/\"bind-address-ipv4\":.*\$/\"bind-address-ipv4\": \"$(ifconfig tun0 | grep 'inet addr:' | cut -d: -f2| cut -d' ' -f1)\",/" /etc/transmission/settings.json
	fi

	transmission-daemon --foreground --config-dir /etc/transmission
fi